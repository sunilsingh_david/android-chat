package android.gcm.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import android.gcm.server.models.GCMPayloadModel;
import android.gcm.server.models.GCMPayloadModel.Data;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebServlet("/d2d")
public class GCMNotificationD2D extends HttpServlet {
	
private static final long serialVersionUID = 1L;
	
	private static final String GOOGLE_SERVER_KEY = "AIzaSyCugk9WiFzIihpRmcZMC1FHTdtjH65xRGA";
	private static final String GCM_ENDPOINT = "https://gcm-http.googleapis.com/gcm/send";
	private static final String REGISTER_NAME = "name";
	private static final String MESSAGE_KEY = "message";
	private static final String TO_NAME = "to";
	private static final String REG_ID_STORE = "GCMRegId.txt";
	
	private ObjectMapper mapper;
	
	public GCMNotificationD2D() {
		super();
	}
	
	@Override
	public void init() {
		mapper = new ObjectMapper();
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		System.out.println("GET method called on GCMNotificationD2D servlet");
		doPost(request, response);
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		System.out.println("POST method called on GCMNotificationD2D servlet");
		String action = request.getParameter("action");
		System.out.println("action = " + action);
		System.out.println("name = " + request.getParameter("name"));
		System.out.println("regToken = " + request.getParameter("regToken"));
		
		if(action.equalsIgnoreCase("shareRegId")) {
			writeToFile(request.getParameter("name"), request.getParameter("regToken"));
		} else if(action.equalsIgnoreCase("sendMessage")) {
			String toToken = null;
			HttpsURLConnection connection = null;
			BufferedReader reader = null;
			try {
				String from = request.getParameter(REGISTER_NAME);
				String to = request.getParameter(TO_NAME);
				String message = request.getParameter(MESSAGE_KEY);
				
				reader = new BufferedReader(new FileReader("GCMRegId.txt"));
				String line;
				while((line = reader.readLine()) != null) {
					String[] parts = line.split(",");
					if(parts[0].equals(to)) {
						toToken = parts[1];
						break;
					}
				}
				
				URL url = new URL(GCM_ENDPOINT);
				connection = (HttpsURLConnection) url.openConnection();
				connection.setDoOutput(true);
				connection.setRequestMethod("POST");
				connection.setRequestProperty("Authorization", "key=" + GOOGLE_SERVER_KEY);
				connection.setRequestProperty("Content-Type", "application/json");
				
				OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
				writer.write(constructRequest(toToken, message));
				writer.close();
				
				int status = connection.getResponseCode();
				if(status == 200) {
					System.out.println("The message was successfully sent to GCM connection server");
				} else {
					System.out.println("Post Failure! Status: " + status);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				if(reader != null) {
					reader.close();
				}
				if(connection != null) {
					connection.disconnect();
				}
			}
		}
	}
	
	private void writeToFile(String name, String token) throws IOException {
		System.out.println("Writing GCM registration token to file");
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(REG_ID_STORE, true)));
		// Map<String, String> map = readFromFile();
		Map<String, String> map = new HashMap<String, String>();
		map.put(name, token);
		// PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(REG_ID_STORE, false)));
		for(Entry<String, String> entry : map.entrySet()) {
			System.out.println("Writing GCM token to file");
			writer.println(entry.getKey() + "," + entry.getValue());
		}
		writer.println();
		writer.close();
	}
	
	private Map<String, String> readFromFile() throws IOException {
		System.out.println("Reading GCMRegId.txt");
		BufferedReader reader = new BufferedReader(new FileReader(REG_ID_STORE));
		String line = "";
		Map<String, String> map = new HashMap<String, String>();
		while((line = reader.readLine()) != null) {
			System.out.println("There are entries in GCMRegId.txt");
			String[] parts = line.split(",");
			map.put(parts[0], parts[1]);
			System.out.println("username = " + parts[0] + "; token = " + parts[1]);
		}
		reader.close();
		return map;
	}
	
	private String constructRequest(String to, String message) {
		GCMPayloadModel gcmPayload = new GCMPayloadModel();
		Data data = new Data();
		
		gcmPayload.setTo(to);
		if(message == null) {
			data.setMessage("Device registered successfully with server");
		} else {
			data.setMessage(message);
		}
		gcmPayload.setData(data);
		
		String payload = null;
		try {
			payload = mapper.writeValueAsString(gcmPayload);
		} catch (JsonProcessingException ex) {
			ex.printStackTrace();
		}
		return payload;
	}

}
