package com.android.gcm.services;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

public class GcmMessageListener extends GcmListenerService {

    private static final String TAG = "GcmMessageListener";

    public GcmMessageListener() {
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.d(TAG, "Message received from GCM server");
        String message = data.getString("message");
        Log.d(TAG, "Message: " + message);
    }
}
