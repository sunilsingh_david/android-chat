package com.android.gcm;

import android.util.Log;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Sunil on 6/20/2015.
 */
public class ShareExternalServer {

    private static final String TAG = "ShareExternalServer";

    public String shareRegIdWithAppServer(final String regToken, final String username) {
        String result = "";
        Map<String, String> paramsMap = new HashMap<String, String>();
        paramsMap.put("action", "shareRegId");
        paramsMap.put("regToken", regToken);
        paramsMap.put(Config.REGISTER_NAME, username);
        result = request(paramsMap);
        if(result.equalsIgnoreCase("success")) {
            result = "RegId shared with GCM application server successfully. RegId: " +
                        regToken + ". Username: " + username;
        }
        Log.d(TAG, "Result: " + result);
        return result;
    }

    public String sendMessage(final String from, final String to, final String message) {
        String result = "";
        Map<String, String> map = new HashMap<String, String>();
        map.put(Config.REGISTER_NAME, from);
        map.put(Config.TO_NAME, to);
        map.put(Config.MESSAGE_KEY, message);
        result = request(map);
        if(result.equalsIgnoreCase("success")) {
            result = "Message sent to user: " + to + " successfully";
        }
        Log.d(TAG, "Result: " + result);
        return result;
    }



    public String request(Map<String, String> paramsMap) {
        String result = "";
        URL url = null;
        OutputStreamWriter writer = null;
        HttpURLConnection connection = null;
        try {
            url = new URL(Config.url);
            StringBuilder postBody = new StringBuilder();
            Iterator iterator = paramsMap.entrySet().iterator();
            while(iterator.hasNext()) {
                Map.Entry param = (Map.Entry) iterator.next();
                postBody.append(param.getKey()).append('=').append(param.getValue());
                if(iterator.hasNext()) {
                    postBody.append('&');
                }
            }
            String payload = postBody.toString();

            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
            writer = new OutputStreamWriter(connection.getOutputStream());
            Log.d(TAG, "Payload = " + payload);
            writer.write(payload);
            writer.flush();

            int status = connection.getResponseCode();
            if(status == 200) {
                Log.d(TAG, "POST succeeded");
                result = "success";
            } else {
                result = "Post Failure. Status: " + status;
                Log.e(TAG, result);
            }
        } catch (MalformedURLException ex) {
            Log.e(TAG, ex.getMessage());
        } catch (IOException ex) {
            Log.e(TAG, ex.getMessage());
        } finally {
            if(connection != null) {
                connection.disconnect();
            }
            if(writer != null) {
                try {
                    writer.close();
                } catch (IOException ex) {

                }
            }
        }
        return result;
    }
}
