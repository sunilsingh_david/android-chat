package com.android.gcm;

/**
 * Created by Sunil on 6/20/2015.
 */
public interface Config {

    public static final String url = "http://192.168.0.3:8085/android-gcm/d2d";

    public static final String REGISTER_NAME = "name";
    public static final String TO_NAME = "to";
    public static final String MESSAGE_KEY = "message";
}
