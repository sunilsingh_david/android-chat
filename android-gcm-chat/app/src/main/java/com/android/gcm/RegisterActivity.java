package com.android.gcm;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = "RegisterActivity";

    private Button btnGCMRegister;
    private Button btnAppRegister;
    private EditText username;

    private String regToken;
    private Context context;

    private ShareExternalServer server;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        context = this;
        server = new ShareExternalServer();

        btnGCMRegister = (Button) findViewById(R.id.btnGCMRegister);
        btnGCMRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetGCMTokenTask().execute();
            }
        });

        username = (EditText) findViewById(R.id.username);
        btnAppRegister = (Button) findViewById(R.id.btnAppRegister);
        btnAppRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RegisterTask().execute();
            }
        });
    }


    private class GetGCMTokenTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            InstanceID instanceID = InstanceID.getInstance(context);
            try {
                regToken = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                Log.d(TAG, "GCM regToken: " + regToken);
            } catch (IOException ex) {
                Log.e(TAG, ex.getMessage());
            }
            return regToken;
        }

        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(context, "GCM Token: " + regToken, Toast.LENGTH_LONG).show();
        }
    }

    private class RegisterTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            Log.d(TAG, "Registering device with app server");
            server.shareRegIdWithAppServer(regToken, username.getText().toString());
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Toast.makeText(context, "Registered successfully with GCM App server", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra(Config.REGISTER_NAME, username.getText().toString());
            startActivity(intent);
        }
    }

}
