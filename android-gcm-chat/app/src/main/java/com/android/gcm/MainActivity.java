package com.android.gcm;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private ShareExternalServer server;
    private String username;
    private Context context;

    private EditText to;
    private EditText message;
    private Button send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        server = new ShareExternalServer();
        username = getIntent().getStringExtra(Config.REGISTER_NAME);
        send = (Button) findViewById(R.id.send);
        to = (EditText) findViewById(R.id.to);
        message = (EditText) findViewById(R.id.message);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SendMessageTask().execute();
            }
        });
    }

    private class SendMessageTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            String result = server.sendMessage(username, to.getText().toString(), message.getText().toString());
            Log.d(TAG, "Result: " + result);
            return result;
        }

        @Override
        protected void onPostExecute(String message) {
            Log.d(TAG, "Result: " + message);
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

}
